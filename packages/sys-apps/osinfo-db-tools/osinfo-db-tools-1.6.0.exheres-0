# Copyright 2018-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pagure [ pn=libosinfo pnv=${PNV} ] \
    python [ blacklist=2 multibuild=false ] \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="Tools for managing the libosinfo database files"
HOMEPAGE+=" https://${PN}.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( linguas: af am anp ar as ast bal be bg bn_IN bn bo br brx bs ca cs cy da de_CH de el en_GB eo
               es et eu fa fi fr gl gu he hi hr hu ia id ilo is it ja ka kk km mn ko kw_GB kw
               kw@kkcor kw@uccor ky lt lv mai mk ml mn mr ms nb nds ne nl nn nso or pa pl pt_BR pt
               ro ru si sk sl sq sr sr@latin sv ta te tg th tr tw uk ur vi wba yo zh_CN zh_HK zh_TW
               zu )
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.9.0]
    build+run:
        app-arch/libarchive[>=3.0.0]
        core/json-glib
        dev-libs/glib:2[>=2.44]
        dev-libs/libxml2:2.0[>=2.6.0]
        dev-libs/libxslt[>=1.0.0]
        gnome-desktop/libsoup:2.4
        !dev-libs/libosinfo:1.0[<1.0.0] [[
            description = [ libosinfo tools reside in a separate package now ]
            resolution = uninstall-blocked-after
        ]]
    test:
        dev-python/pytest[python_abis:*(-)?]
        dev-python/requests[python_abis:*(-)?]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --disable-static
)

src_prepare() {
    # Respect selected python abi
    edo sed \
        -e "s:python3:python$(python_get_abi):g" \
        -i configure.ac

    autotools_src_prepare
}

