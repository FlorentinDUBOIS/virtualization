# Copyright 2011 Brett Witherspoon <spoonb@exherbo.org>
# Copyright 2012-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service udev-rules test-dbus-daemon
require vala [ with_opt=true ]
require meson

SUMMARY="A Gtk client and libraries for SPICE remote desktop servers."

HOMEPAGE="http://spice-space.org/page/Spice-Gtk"
DOWNLOADS="http://www.spice-space.org/download/gtk/${PNV}.tar.bz2"

LICENCES="LGPL-2.1"
SLOT="3.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    lz4 [[ description = [ Enable lz4 compression ] ]]
    polkit [[ description = [ Enable PolicyKit support (for the usb acl helper ] ]]
    sasl [[ description = [ use cyrus SASL for authentication ] ]]
    smartcard [[ description = [ Enable smartcard support ] ]]
    usbredir [[ description = [ Enable USB redirection support ] ]]

    vapi [[ requires = [ gobject-introspection ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-python/pyparsing
        sys-devel/gettext[>=0.18.2]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.4] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.14] )
    build+run:
        core/json-glib
        dev-libs/glib:2[>=2.46]
        dev-libs/libepoxy
        media-libs/celt:0.5.1[>=0.5.1.1]
        (
            media-libs/gstreamer:1.0[>=1.10.0][gobject-introspection?]
            media-plugins/gst-libav[>=1.10.0]
            media-plugins/gst-plugins-bad:1.0[>=1.10.0]
            media-plugins/gst-plugins-base:1.0[>=1.10.0]
            media-plugins/gst-plugins-good:1.0[>=1.10.0]
        )
        media-libs/opus[>=0.9.14]
        sys-apps/usbutils-data
        virtualization-lib/spice-protocol[>=0.12.15]
        x11-dri/libdrm [[ note = [ src/spice-widget-egl.c uses libdrm/drm_fourcc.h ] ]]
        x11-libs/cairo[>=1.2.0]
        x11-libs/gdk-pixbuf:2.0[>=2.26.0]
        x11-libs/gtk+:3[>=3.22]
        x11-libs/libva [[ note = [ Automagic ] ]]
        x11-libs/libX11
        x11-libs/pixman:1[>=0.17.7]
        lz4? ( app-arch/lz4 )
        polkit? (
            sys-apps/acl
            sys-auth/polkit:1[>=0.96]
        )
        sasl? ( net-libs/cyrus-sasl )
        smartcard? ( dev-libs/libcacard[>=2.5.1] )
        usbredir? (
            dev-libs/usbredir[>=0.7.1]
            virtual/usb:1
        )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        !virtualization-ui/spice-gtk2:2.0 [[
            description = [ Install the same things ]
            resolution = uninstall-blocked-after
        ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dbuiltin-mjpeg=true
    -Dcoroutine=ucontext

    -Dcelt501=enabled
    -Dgtk=enabled
    -Dopus=enabled

    -Dpulse=disabled
    -Dwebdav=disabled
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
    lz4
    polkit
    sasl
    smartcard
    usbredir
    vapi
)

src_prepare() {
    meson_src_prepare

    # Disable usb-acl-helper test, the `mock-acl-helper` process isn't terminated properly and keeps
    # the tests hanging forever
    edo sed -e "/'usb-acl-helper.c',/d" \
            -i tests/meson.build
}

src_test() {
    test-dbus-daemon_run-tests meson_src_test
}

