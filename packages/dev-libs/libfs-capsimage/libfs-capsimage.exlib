# Copyright 2012-2015 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.12 1.11 ] ]

export_exlib_phases src_prepare

SUMMARY="IPF User Library (CAPSImage)"
DESCRIPTION="
User library for IPF (Interchangeable Preservation Format) files which are used
to preserve floppy disk images.
The IPF format is:
- platform agnostic.
- low-level, representing the information as it would have been read by a drive head.
- not just for disks: Any type of digital media can be contained by the format.
"
HOMEPAGE="http://www.softpres.org"
DOWNLOADS="http://fengestad.no/fs-uae/files/${PNV}.tar.gz"
LICENCES="IPF"
SLOT="0"
MYOPTIONS=""
DEPENDENCIES=""

WORK=${WORKBASE}/${PNV}/CAPSImage

DEFAULT_SRC_INSTALL_EXTRA_PREFIXES=( ../ )
DEFAULT_SRC_INSTALL_EXTRA_DOCS=( DONATIONS.txt RELEASE.txt )

libfs-capsimage_src_prepare() {
    # Fix package name.
    edo sed -i -e "s:^\(PACKAGE\)=.*:\1=fs-capsimage:" configure.in

    # Respect ?FLAGS & use -fpermissive.
    edo sed -i -e "/\bCFLAGS/d" configure.in
    edo sed -i -e "/\bCXXFLAGS/d" configure.in
    edo sed -i -e "/AC_SUBST(PACKAGE)/iCFLAGS=\"${CFLAGS} -fpermissive\"" configure.in
    edo sed -i -e '/AC_SUBST(PACKAGE)/iCXXFLAGS=${CFLAGS}' configure.in

    # Fix lib & include dirs
    edo sed -i -e "s:\(/usr\)/lib:\1/$(exhost --target)/lib:g" Makefile.in
    edo sed -i -e "s:\(/usr\)/include:\1/$(exhost --target)/include:g" Makefile.in

    eautoreconf
}

